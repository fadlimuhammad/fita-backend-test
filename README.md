# Fita Backend Test - Appointment



## Getting started
### how to run: 
- install library with ``` npm install ```
- run migration table using ``` npx sequelize-cli db:migrate ```
- to run unit test use ``` npm run test ```
- to run app use ``` npm run start ```


### flow appointment
the app very dependency to library date manipulation like ``` moment js ``` or ``` day js ```. To validate a appointment im convert from timezone coach to local server for create or validate a appointment. use helper function from ``` moment js ``` like ``` isSameOrBefore() or isSameOrAfter() ``` to validate existing appointment that have with coach. lastly im using sqlite for persistent db to store or compare list of appointment coach.

to test api i just create http client script with name ``` test.http ```. you can it for test api.
Thankyou very much for the opportunity, i cant wait to grow up with people of fita.

Best regard, Fadli.