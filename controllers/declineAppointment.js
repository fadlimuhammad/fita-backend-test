const declineAppointment = require('../useCase/declineAppointment');
const wrapResponse = require('../utils/wrapResponse');

module.exports = async (req, res, next) => {
  const {body} = req;

  try {
    const useCaseResult = await declineAppointment(body);
    return wrapResponse(res, useCaseResult);
  } catch (error) {
    next(error);
  }
};
