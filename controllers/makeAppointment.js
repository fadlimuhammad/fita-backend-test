const makeAppointmentUseCase = require('../useCase/makeAppointment');
const wrapResponse = require('../utils/wrapResponse');

module.exports = async (req, res, next) => {
  const {body} = req;

  try {
    const useCaseResult = await makeAppointmentUseCase(body);
    return wrapResponse(res, useCaseResult);
  } catch (error) {
    next(error);
  }
};
