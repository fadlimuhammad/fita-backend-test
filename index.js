const http = require('http');
const express = require('express');
const routes = require('./routes');


const app = express();
app.use(express.json());
app.use('/api/v1', routes);

// handle error 500
// eslint-disable-next-line no-unused-vars
app.use(function (err, req, res, _next) {
  console.log(err);
  res.status(500);
  res.json({
    message: err.message
  });
});

const port = process.env.APP_PORT || '3001';
app.set('port', port);
const server = http.createServer(app);
server.listen(port);
server.on('listening', () => {
  console.info('app-startup', `🚀 Server start on port ${port}. Running with ${process.env.NODE_ENV || 'development'} environment...`);
});

