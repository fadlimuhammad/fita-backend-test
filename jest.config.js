module.exports = async () => {
  return {
    verbose: true,
    collectCoverageFrom: ['**/*.{js,jsx}', '!**/node_modules/**'],
    coverageDirectory: '<rootDir>/coverage',
    coverageReporters: ['lcov'],
    coveragePathIgnorePatterns: [
      '<rootDir>/node_modules/',
      '<rootDir>/docs/',
      '<rootDir>/logs/',
      '<rootDir>/src/config/'
    ],
    coverageThreshold: {
      global: {
        branch: 80,
        functions: 80,
        lines: 80,
        statements: 80
      }
    },
    roots: ['<rootDir>/tests/'],
    testEnvironment: 'node',
    testMatch: ['**/?(*.)+(spec|test).[tj]s?(x)']
  };
};
