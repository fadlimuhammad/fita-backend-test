
const model = require('../models');

const insert = async (data) => {
  return model.appointment.create(data);
};

const findAll = async (where) => {
  return model.appointment.findAll({where});
};

const destroy = async (where) => {
  return model.appointment.destroy({where});
};

module.exports = {
  insert,
  findAll,
  destroy,
};
