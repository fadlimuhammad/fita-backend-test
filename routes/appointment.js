const router = require('express').Router();
const makeAppointmentController = require('../controllers/makeAppointment');
const declineAppointmentController = require('../controllers/declineAppointment');

router.post('/', makeAppointmentController);
router.post('/decline', declineAppointmentController);

module.exports = router;
