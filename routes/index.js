const router = require('express').Router();
const appointmentRoute = require('./appointment');

router.get('/server-health', (req, res) => res.status(200).json({
  message: 'all good',
  date: new Date(),
}));
router.use('/appointment', appointmentRoute);
module.exports = router;
