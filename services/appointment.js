const moment = require('moment-timezone');
const processCSVFile = require('./processCSVFile');
const appointmentRepository = require('../repository/appointment');

const validateAppointment = (availabilities, appointmentDateTime, existingAppointments) => {
  const sortedAvailabilities = availabilities.sort((a, b) => moment(a.localAvailableStart).valueOf() - moment(b.localAvailableStart).valueOf());

  for (const availability of sortedAvailabilities) {
    const coachTimeStart = moment(availability.localAvailableStart);
    const coachTimeEnd = moment(availability.localAvailableEnd);

    const appointmentDay = appointmentDateTime.startTime.format('dddd');
    // check avialable day
    if (availability.day_of_week === appointmentDay) {
      const appointmentDuration = moment.duration(appointmentDateTime.startTime.diff(appointmentDateTime.endTime));

      // check available duration
      if (Math.abs(coachTimeEnd.diff(coachTimeStart)) >= appointmentDuration.asMilliseconds()) {

        // check is conflict with existingAppointments
        const isConflict = existingAppointments.some(existing => {
          const existingStart = moment(existing.startTime);
          const existingEnd = moment(existing.endTime);

          return (appointmentDateTime.startTime.isSameOrBefore(existingEnd) && appointmentDateTime.endTime.isSameOrAfter(existingStart));
        });

        if(!isConflict) {
          return appointmentDateTime;
        }
      }
    }
  }
  return null;
};

const makeAppointment = async (filePath, coachName, appointmentDateTime, existingAppointments) => {
  return new Promise((resolve, reject) => {
    processCSVFile(filePath, (coachAvailabilities) => {
      coachAvailabilities = coachAvailabilities.map(coach => {
        coach.localAvailableStart = moment(`${coach.day_of_week} ${coach.available_at} ${coach.timezone.split(' ')[0]}`, 'dddd LT Z');
        coach.localAvailableEnd = moment(`${coach.day_of_week} ${coach.available_until} ${coach.timezone.split(' ')[0]}`, 'dddd LT Z');
        return coach;
      }).filter(coach => coach.name === coachName);
      const validationMessage = validateAppointment(coachAvailabilities, appointmentDateTime, existingAppointments);

      if (validationMessage) {
        appointmentRepository.insert({...appointmentDateTime, coachName}).then(() => resolve('Appointment has been made successfully.'));
      } else {
        resolve('Invalid appointment: no available time slots found');
      }
    }).catch(() => reject('Error when make appointment'));
  });
};

module.exports = {
  validateAppointment,
  makeAppointment,
};
