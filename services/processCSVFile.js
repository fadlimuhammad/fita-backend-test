const fs = require('fs');
const csv = require('csv-parser');

module.exports = async (filePath, callback) => {
  const appointments = [];

  fs.createReadStream(filePath)
    .pipe(csv({mapHeaders: ({ header }) => header.toLowerCase().replace(/ /g, '_')}))
    .on('data', (row) => {
      appointments.push(row);
    })
    .on('end', () => {
      callback(appointments);
    })
    .on('error', (error) => {
      console.error('Error while processing CSV file:', error);
    });
};
