const moment = require('moment-timezone');
const appointmentService = require('../services/appointment');
const appointmentRepository = require('../repository/appointment');
const declineUseCase = require('../useCase/declineAppointment');

jest.mock('../repository/appointment');

jest.setTimeout(60000);
describe('make appointment base on available coach', () => {
  const existingAppointments = [
    {
      startTime: moment('Monday 10:00PM', 'dddd LT'),
      endTime: moment('Monday 10:30PM', 'dddd LT'),
    },
    {
      startTime: moment('Monday 01:00AM', 'dddd LT'),
      endTime: moment('Monday 01:30AM', 'dddd LT'),
    }
  ];

  it('should make a appointment', async () => {
    const appointmentDateTime = {
      startTime: moment('Monday 09:30PM', 'dddd LT'),
      endTime: moment('Monday 09:59PM', 'dddd LT')
    };

    const mock = jest.spyOn(appointmentRepository, 'insert');
    mock.mockImplementation(data => {
      return new Promise(resolve => {
        existingAppointments.push({startTime: data.startTime, endTime: data.endTime});
        resolve(true);
      });
    });
    const appointmentServiceResult = await appointmentService.makeAppointment('./storage/data.csv', 'Christy Schumm', appointmentDateTime, existingAppointments);
    expect(appointmentServiceResult).toBeTruthy();
    expect(existingAppointments.length).toEqual(3);

  });

  it('should decline appointment and rechedule', async () => {
    const mockInsert = jest.spyOn(appointmentRepository, 'insert');
    mockInsert.mockImplementation(data => {
      return new Promise(resolve => {
        existingAppointments.push({startTime: data.startTime, endTime: data.endTime});
        resolve(true);
      });
    });

    const mockFindall = jest.spyOn(appointmentRepository, 'findAll');
    mockFindall.mockImplementation(() => {
      return new Promise(resolve => {
        resolve(existingAppointments);
      });
    });

    const mockDestroy = jest.spyOn(appointmentRepository, 'destroy');
    mockDestroy.mockImplementation(() => {
      return new Promise(resolve => {
        existingAppointments.pop();
        resolve(existingAppointments);
      });
    });

    const declineRequest = {
      reschedule: true,
      rescheduleData: {
        day: 'Monday',
        startTime: '08:00PM',
        endTime: '09:00PM',
        coachName: 'Christy Schumm'
      }
    };
    const declineResult = await declineUseCase(declineRequest);

    expect(mockInsert).toHaveBeenCalled();
    expect(mockFindall).toHaveBeenCalled();
    expect(mockDestroy).toHaveBeenCalled();
    expect(declineResult).toBeTruthy();
    expect(existingAppointments.length).toEqual(3);
  });
});

describe('Edge case where coach not available', () => {
  const existingAppointments = [
    {
      startTime: moment('Monday 10:00PM', 'dddd LT'),
      endTime: moment('Monday 10:30PM', 'dddd LT'),
    },
    {
      startTime: moment('Monday 01:00AM', 'dddd LT'),
      endTime: moment('Monday 01:30AM', 'dddd LT'),
    }
  ];

  it('should not make a appointment', async () => {
    const appointmentDateTime = {
      startTime: moment('Monday 01:25AM', 'dddd LT'),
      endTime: moment('Monday 03:00AM', 'dddd LT')
    };

    const mock = jest.spyOn(appointmentRepository, 'insert');
    mock.mockImplementation(data => {
      return new Promise(resolve => {
        existingAppointments.push({startTime: data.startTime, endTime: data.endTime});
        resolve(true);
      });
    });
    const appointmentServiceResult = await appointmentService.makeAppointment('./storage/data.csv', 'Christy Schumm', appointmentDateTime, existingAppointments);
    expect(appointmentServiceResult).toBeTruthy();
    expect(appointmentServiceResult).toEqual('Invalid appointment: no available time slots found');
    expect(existingAppointments.length).toEqual(2);

  });
});
