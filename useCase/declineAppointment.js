const moment = require('moment-timezone');
const appointmentRepository = require('../repository/appointment');
const appointmentService = require('../services/appointment');
const wrapReturnUseCase = require('../utils/wrapReturnUseCase');

module.exports = async (appointmentRequest) => {
  const appointmentId = appointmentRequest.appointmentId;
  const isReschedule = appointmentRequest.reschedule;
  let appointmentMsg = 'Appointment has been decline';

  await appointmentRepository.destroy({id: appointmentId});
  if (isReschedule) {
    const rescheduleData = appointmentRequest.rescheduleData;
    const coachName = rescheduleData.coachName;
    const appointmentData = {
      startTime: moment(`${rescheduleData.day} ${rescheduleData.startTime}`, 'dddd LT'),
      endTime: moment(`${rescheduleData.day} ${rescheduleData.endTime}`, 'dddd LT'),
    };
    const existingAppointments = await appointmentRepository.findAll({coachName});
    appointmentMsg = await appointmentService.makeAppointment('./storage/data.csv', coachName, appointmentData, existingAppointments);
  }
  return wrapReturnUseCase(true, null, appointmentMsg);
};
