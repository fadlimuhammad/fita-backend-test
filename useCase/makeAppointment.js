const moment = require('moment-timezone');
const appointmentRepository = require('../repository/appointment');
const appointmentService = require('../services/appointment');
const wrapReturnUseCase = require('../utils/wrapReturnUseCase');

module.exports = async (appointmentRequest) => {
  const coachName = appointmentRequest.coachName;
  const appointmentData = {
    startTime: moment(`${appointmentRequest.day} ${appointmentRequest.startTime}`, 'dddd LT'),
    endTime: moment(`${appointmentRequest.day} ${appointmentRequest.endTime}`, 'dddd LT'),
  };
  const existingAppointments = await appointmentRepository.findAll({coachName});
  const appointmentMsg = await appointmentService.makeAppointment('./storage/data.csv', coachName, appointmentData, existingAppointments);
  return wrapReturnUseCase(true, null, appointmentMsg);
};
